# Based on

https://github.com/LukeSmithxyz/voidrice/blob/master/.scripts/SCRIPTS.md

export PATH="$(du $HOME/.scripts/ | cut -f2 | tr '\n' ':')$PATH"

# Dependencies

```
    pacman -S youtube-dl mpv the_silver_searcher termite offlineimap python-virtualenv i3blocks yaourt -S community/newsboat community/the_silver_searcher
```

## dmenu

    Scripts used in dmenu

    - `background_music`: Play music in background


## System

    System scripts

    - `qnap_start`: Mount shared folder from NAS
    - `qnap_stop`: Unmount shared folder from NAS


community/sysstat aur/mu
